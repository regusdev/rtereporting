﻿using System;
using System.Reflection;
using log4net;
using log4net.Core;
using log4net.Repository;

namespace Logging
{
    public class Logger : ILogger
    {
        public static readonly ILog log =
            LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public string Name
        {
            get { throw new NotImplementedException(); }
        }

        public ILoggerRepository Repository
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsEnabledFor(Level level)
        {
            throw new NotImplementedException();
        }

        public void Log(LoggingEvent logEvent)
        {
            throw new NotImplementedException();
        }

        public void Log(Type callerStackBoundaryDeclaringType, Level level, object message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Log(string logDetails, int level)
        {
            log.Info(logDetails);
        }
    }
}