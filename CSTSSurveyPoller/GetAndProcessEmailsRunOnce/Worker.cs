﻿using EmailOperations;
using Logging;
using ProcessEmail;
using Rebex.Net;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace GetAndProcessEmailsRunOnce
{
    public class Worker : IWorker
    {
        private ImapHelper _helper;
        private List<MyMessage> _emailList;
        private readonly Logger _logger;

        public Worker(Logger logger)
        {
            _logger = logger;
        }

        public void DoWork()
        {
            try
            {
                _helper = EmailOps.ConnectHelper();
            }
            catch (ImapException exception)
            {
                _logger.Log(string.Format("Can't Connect to Mailbox: " + exception), 0);
                Environment.Exit(0);
            }

            try
            {
                _emailList = EmailOps.GetMyMessages(_helper);
            }
            catch (ImapException exception)
            {
                _logger.Log(string.Format("Can't Get Messages: " + exception), 0);
                Environment.Exit(0);

            }

            _logger.Log("number of Emails: " + _emailList.Count, 1);
            var index = 0;

            foreach (var message in _emailList)
            {
                try
                {
                    _logger.Log("Message #" + index + " " + message.Message.Subject, 1);
                    index++;
                    if (message.Message.From[0].Address == "regushr.workflow@regus.com")
                    {
                        var _processEmail = new ProcessEmailMethods(_logger);

                        var empDetails = _processEmail.GetPeopleSoftEmployeeDetails(message.Message.BodyText,
                        message.Message.Date.OriginalTime, message.Message.From.ToString(), message.Message.Subject);
                        _logger.Log("Create starter request user: " + empDetails.Forename + " " + empDetails.Surname, 1);

                        var createSam = _processEmail.CreateHrRequestTask(empDetails);

                        _logger.Log(createSam.Result, 1);
                    }
                    else
                    {
                        //needs refactoring to move email rather than delete it.
                        _logger.Log("Unknown mail type - Deleting and Skipping Processing", 1);
                    }


                    if (ConfigurationManager.AppSettings.Get("DeleteEmailOnRead") == "True")
                    {
                        EmailOps.DeleteMessage(_helper, message.UniqueId);
                    }
                }
                catch (IndexOutOfRangeException exception)
                {
                    _logger.Log("Error In create: " + exception, 0);
                }

            }
            _helper.Purge();

        }

    }

}