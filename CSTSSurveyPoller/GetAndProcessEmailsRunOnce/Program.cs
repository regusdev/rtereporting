﻿using Logging;
using System;


namespace GetAndProcessEmailsRunOnce
{
    public interface IWorker
    {
        void DoWork();
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Global exception handler.
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            var worker = new Worker(new Logger());
            worker.DoWork();
        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new Logger().Log("-Global unhandled exception:" + e.ExceptionObject, 0);
            // Log exception / send notification.
        }
    }

}
