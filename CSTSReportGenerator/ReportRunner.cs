﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Logging;
using RTEReportGenerator;

namespace CSTSReportGenerator
{
    public class reportAttributes
    {
        public string ReportExecDetail { get; set; }
        public string OutputPath { get; set; }
    }

    public class ReportRunner
    {
        private const int RowsPerSheet = 300000;
        private static readonly DataTable ResultsData = new DataTable();
        private static readonly Logger Logger = new Logger();

        public static void Execute()
        {
            Logger.Log("--Starting report Run--", 0);
            Console.WriteLine("--Starting report Run--");
            var currentDate = DateTime.Now;

            var reportsToRun = new List<reportAttributes>
            {
                new reportAttributes
                {
                    //Run Daily
                    ReportExecDetail = "EXEC[bpmrpt].[SummaryAnalysis] 3, 3, 0,0",
                    OutputPath = @"\Daily\prod"
                }
            };

            reportsToRun.Add(new reportAttributes
            {
                //Accounts Recievable Report
                ReportExecDetail = "Exec [bpm].[RTEARDailyExtract];",
                OutputPath = @"\AR"
            }
                );


            var day = (DayOfWeek) currentDate.Day;
            if (day == DayOfWeek.Saturday)
            {
                //Weekly Report
                reportsToRun.Add(new reportAttributes
                {
                    ReportExecDetail = "EXEC[bpmrpt].[SummaryAnalysis] 3, 0, 0,0",
                    OutputPath = @"\Weekly\prod"
                });
            }

            //if (currentDate.Day == 1)
            //{
            //Run Monthly

            var newDate = currentDate.AddMonths(-1);
            reportsToRun.Add(new reportAttributes
            {
                ReportExecDetail =
                    "EXEC [bpmrpt].[SummaryAnalysis] 3, 2, " + newDate.Year + ", " + newDate.ToMonthName(),
                OutputPath = @"\Monthly\prod"
            });
            //}

            foreach (var report in reportsToRun)
            {
                RunReport(report.ReportExecDetail, report.OutputPath);
            }
        }

        private static void RunReport(string report, string outputPath)
        {
            ResultsData.Reset();
            using (var connection =
                new SqlConnection(@"Data Source=reg10vss02bpm.regus.local;" +
                                  @"Initial Catalog=SORBPM;User Id=bpm_sa;Password=bpm_sa"))
            {
                var command = new SqlCommand(report, connection);
                connection.Open();
                command.CommandTimeout = 300;
                Console.WriteLine("--Executing Query-- " + report);
                Logger.Log("--Executing Query-- " + report, 0);
                var reader = command.ExecuteReader();
                Console.WriteLine("--Query finished--");
                Logger.Log("--Query Finished--", 0);
                var c = 0;
                var firstTime = true;

                //Get the Columns names, types, this will help
                //when we need to format the cells in the excel sheet.x
                var dtSchema = reader.GetSchemaTable();
                var listCols = new List<DataColumn>();
                if (dtSchema != null)
                {
                    foreach (DataRow drow in dtSchema.Rows)
                    {
                        var columnName = Convert.ToString(drow["ColumnName"]);
                        var column = new DataColumn(columnName, (Type) (drow["DataType"]))
                        {
                            Unique = (bool) drow["IsUnique"],
                            AllowDBNull = (bool) drow["AllowDBNull"],
                            AutoIncrement = (bool) drow["IsAutoIncrement"]
                        };
                        listCols.Add(column);
                        ResultsData.Columns.Add(column);
                    }
                }

                // Call Read before accessing data.
                while (reader.Read())
                {
                    var dataRow = ResultsData.NewRow();
                    for (var i = 0; i < listCols.Count; i++)
                    {
                        dataRow[(listCols[i])] = reader[i];
                    }
                    ResultsData.Rows.Add(dataRow);
                    c++;
                    if (c == RowsPerSheet)
                    {
                        c = 0;
                        ExportToOxml(firstTime, outputPath);
                        ResultsData.Clear();
                        firstTime = false;
                    }
                }
                if (ResultsData.Rows.Count > 0)
                {
                    Logger.Log("Number of Rows: " + ResultsData.Rows.Count, 0);
                    ExportToOxml(firstTime, outputPath);
                    ResultsData.Clear();
                }
                // Call Close when done reading.
                reader.Close();
                Console.WriteLine("--Done--");
                Logger.Log("--Report Run Complete--", 0);
            }
        }

        private static void ExportToOxml(bool firstTime, string outputPath)
        {
            var oNetworkDrive = new NetworkDrive();
            oNetworkDrive.LocalDrive = "l:";

            try
            {
                oNetworkDrive.UnMapDrive();
            }
            catch
            {
                // ignored
            }

            oNetworkDrive.ShareName =
                @"\\reg10fil03.regus.local\bpm$\Business Reporting\RTE\Business Reporting\global\Summary Analysis" +
                outputPath;
            oNetworkDrive.MapDrive();

            var yesterday = DateTime.Now.Date.AddDays(-1);
            var fileDate = yesterday.ToString("yyyyMMdd");
            var fileName = "";
            fileName = @"l:\Summary_" + fileDate + ".xlsx";


            Logger.Log("--Report File: " + fileName, 0);

            //Delete the file if it exists.
            if (firstTime && File.Exists(fileName))
            {
                Logger.Log("-- Deleting Report File: " + fileName, 0);
                File.Delete(fileName);
            }

            uint sheetId = 1; //Start at the first sheet in the Excel workbook.

            if (firstTime)
            {
                //This is the first time of creating the excel file and the first sheet.
                // Create a spreadsheet document by supplying the filepath.
                // By default, AutoSave = true, Editable = true, and Type = xlsx.
                var spreadsheetDocument = SpreadsheetDocument.
                    Create(fileName, SpreadsheetDocumentType.Workbook);

                // Add a WorkbookPart to the document.
                var workbookpart = spreadsheetDocument.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart.
                var worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);

                // Add Sheets to the Workbook.
                var sheets = spreadsheetDocument.WorkbookPart.Workbook.
                    AppendChild(new Sheets());

                // Append a new worksheet and associate it with the workbook.
                var sheet = new Sheet
                {
                    Id = spreadsheetDocument.WorkbookPart.
                        GetIdOfPart(worksheetPart),
                    SheetId = sheetId,
                    Name = "Sheet" + sheetId
                };
                sheets.Append(sheet);

                //Add Header Row.
                var headerRow = new Row();
                foreach (DataColumn column in ResultsData.Columns)
                {
                    var cell = new Cell
                    {
                        DataType = CellValues.String,
                        CellValue = new CellValue(column.ColumnName)
                    };
                    headerRow.AppendChild(cell);
                }
                sheetData.AppendChild(headerRow);
                var index = 0;
                foreach (DataRow row in ResultsData.Rows)
                {
                    Console.WriteLine("row:" + index);
                    var newRow = new Row();
                    foreach (DataColumn col in ResultsData.Columns)
                    {
                        //added if statement to convert appropriate
                        //columns to number format as per request
                        //TODO: potential re-factor
                        if (col.ColumnName == "TicketRef"
                            || col.ColumnName == "Centre"
                            || col.ColumnName == "Resolution_time_in_work_days"
                            || col.ColumnName == "CompanyId")
                        {
                            var cell = new Cell
                            {
                                DataType = CellValues.Number,
                                CellValue = new CellValue(row[col].ToString())
                            };

                            newRow.AppendChild(cell);
                        }
                        else
                        {
                            var cell = new Cell
                            {
                                DataType = CellValues.String,
                                CellValue = new CellValue(row[col].ToString())
                            };

                            newRow.AppendChild(cell);
                        }
                    }
                    index++;
                    sheetData.AppendChild(newRow);
                }
                Logger.Log("--Saving Report--", 0);
                workbookpart.Workbook.Save();

                spreadsheetDocument.Close();
            }
            else
            {
                // Open the Excel file that we created before, and start to add sheets to it.
                var spreadsheetDocument = SpreadsheetDocument.Open(fileName, true);

                var workbookpart = spreadsheetDocument.WorkbookPart;
                if (workbookpart.Workbook == null)
                    workbookpart.Workbook = new Workbook();

                var worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);
                var sheets = spreadsheetDocument.WorkbookPart.Workbook.Sheets;

                if (sheets.Elements<Sheet>().Any())
                {
                    //Set the new sheet id
                    sheetId = sheets.Elements<Sheet>().Max(s => s.SheetId.Value) + 1;
                }
                else
                {
                    sheetId = 1;
                }

                // Append a new worksheet and associate it with the workbook.
                var sheet = new Sheet
                {
                    Id = spreadsheetDocument.WorkbookPart.
                        GetIdOfPart(worksheetPart),
                    SheetId = sheetId,
                    Name = "Sheet" + sheetId
                };
                sheets.Append(sheet);

                //Add the header row here.
                var headerRow = new Row();

                foreach (DataColumn column in ResultsData.Columns)
                {
                    var cell = new Cell
                    {
                        DataType = CellValues.String,
                        CellValue = new CellValue(column.ColumnName)
                    };
                    headerRow.AppendChild(cell);
                }
                sheetData.AppendChild(headerRow);

                foreach (DataRow row in ResultsData.Rows)
                {
                    var newRow = new Row();

                    foreach (DataColumn col in ResultsData.Columns)
                    {
                        var cell = new Cell
                        {
                            DataType = CellValues.String,
                            CellValue = new CellValue(row[col].ToString())
                        };
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

                workbookpart.Workbook.Save();

                // Close the document.
                spreadsheetDocument.Close();
            }
        }
    }

    internal static class DateTimeExtensions
    {
        public static string ToMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTime.Month);
        }

        public static string ToShortMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(dateTime.Month);
        }
    }
}