﻿using CSTSReportGenerator;
using Logging;
using Quartz;
using Quartz.Impl;
using System;
using RTEReportGenerator;
using Topshelf;
using Topshelf.HostConfigurators;
using Topshelf.Runtime;
using Topshelf.ServiceConfigurators;

namespace RTEReportGenerator
{
    public class ReportScheduler
    {
        public void Start()
        {
            ReportRunner.Execute();
        }

        public void Stop()
        {
            Console.WriteLine("RTE report stopped");
        }
    }
}

public class Program
{
    public static void Main()
    {
        //just run the damn thing...
        ReportRunner.Execute();

        /* HostFactory.Run(x =>                                 //1
         {
             x.Service<ReportScheduler>(s =>                        //2
             {
                 s.ConstructUsing(name => new ReportScheduler());     //3
                 s.WhenStarted(tc => tc.Start());              //4
                 s.WhenStopped(tc => tc.Stop());               //5
             });
             x.RunAsLocalSystem();                            //6

             x.SetDescription("RTE Host");        //7
             x.SetDisplayName("RTE Reporting");                       //8
             x.SetServiceName("RTEReporting");                       //9
         });*/                                                  //10
    }
}